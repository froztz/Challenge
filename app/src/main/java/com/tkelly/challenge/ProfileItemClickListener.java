package com.tkelly.challenge;

import com.tkelly.challenge.application.models.User;

/**
 * Created by Frostz on 8/19/2017.
 */

public interface ProfileItemClickListener {
    void onProfileItemClick(User user);
}
