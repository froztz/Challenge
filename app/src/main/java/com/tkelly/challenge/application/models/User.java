package com.tkelly.challenge.application.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Frostz on 8/19/2017.
 */

public class User implements Parcelable{

    private Double age;
    private String backgroundColor;
    private String hobbies;
    private Boolean isMale;
    private String name;
    private String profileImageUrl;
    private Long uid;
    private String key;

    public User() {
    }

    public User(Double age, String backgroundColor, String hobbies, Boolean isMale, String name, String profileImageUrl, Long uid) {
        this.age = age;
        this.backgroundColor = backgroundColor;
        this.hobbies = hobbies;
        this.isMale = isMale;
        this.name = name;
        this.profileImageUrl = profileImageUrl;
        this.uid = uid;
    }

    public Double getAge() {
        return age;
    }

    public void setAge(Double age) {
        this.age = age;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getHobbies() {
        return hobbies;
    }

    public void setHobbies(String hobbies) {
        this.hobbies = hobbies;
    }

    public Boolean getIsMale() {
        return isMale;
    }

    public void setIsMale(Boolean isMale) {
        this.isMale = isMale;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.age);
        dest.writeString(this.backgroundColor);
        dest.writeString(this.hobbies);
        dest.writeValue(this.isMale);
        dest.writeString(this.name);
        dest.writeString(this.profileImageUrl);
        dest.writeValue(this.uid);
        dest.writeString(this.key);
    }

    protected User(Parcel in) {
        this.age = (Double) in.readValue(Double.class.getClassLoader());
        this.backgroundColor = in.readString();
        this.hobbies = in.readString();
        this.isMale = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.name = in.readString();
        this.profileImageUrl = in.readString();
        this.uid = (Long) in.readValue(Long.class.getClassLoader());
        this.key = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
