package com.tkelly.challenge.application.models;

/**
 * Created by Frostz on 8/19/2017.
 */

public class Strings {
    public static final String MALE = "Male";
    public static final String Female = "Female";
    public static final String NAME = "name";
    public static final String AGE = "age";
    public static final String LOADING = "loading";
}
