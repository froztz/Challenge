package com.tkelly.challenge;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.tkelly.challenge.application.models.Strings;
import com.tkelly.challenge.application.models.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Frostz on 8/19/2017.
 */

public class ProfileRecyclerViewAdapter extends RecyclerView.Adapter<ProfileRecyclerViewAdapter.ViewHolder> {

    private Context context;
    private List<User> userList;
    private ProfileItemClickListener profileItemClickListener;

    public ProfileRecyclerViewAdapter(Context context, List<User> userList, ProfileItemClickListener profileItemClickListener) {
        this.context = context;
        this.userList = userList;
        this.profileItemClickListener = profileItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View profileView = LayoutInflater
                .from(context)
                .inflate(R.layout.card_profile, parent, false);
        return new ViewHolder(profileView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        User user = userList.get(position);
        holder.nameTextView.setText(user.getName());
        holder.ageTextView.setText(String.valueOf(user.getAge().intValue()));
        if (user.getIsMale() != null) {
            holder.backgroundLinearLayout.setBackgroundColor(user.getIsMale() ? context.getResources().getColor(R.color.colorMale): context.getResources().getColor(R.color.colorFemale));
            holder.genderTextView.setText(user.getIsMale() ? Strings.MALE : Strings.Female);
        }
        String uid = "UID: "+String.valueOf(user.getUid());
        holder.uidTextView.setText(uid);
        holder.hobbiesTextView.setText(user.getHobbies());

        if (user.getProfileImageUrl() != null){
            Glide.with(holder.profileImageView.getContext())
                    .load(user.getProfileImageUrl())
                    .into(holder.profileImageView);
        }
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public void refresh(List<User> userList){
        this.userList.clear();
        this.userList.addAll(userList);
        this.notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.card_profile_layout_background)
        LinearLayout backgroundLinearLayout;
        @BindView(R.id.card_profile_image_profile)
        ImageView profileImageView;
        @BindView(R.id.card_profile_text_name)
        TextView nameTextView;
        @BindView(R.id.card_profile_text_age)
        TextView ageTextView;
        @BindView(R.id.card_profile_text_gender)
        TextView genderTextView;
        @BindView(R.id.card_profile_text_uid)
        TextView uidTextView;
        @BindView(R.id.card_profile_text_hobbies)
        TextView hobbiesTextView;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (profileItemClickListener != null){
                        profileItemClickListener.onProfileItemClick(userList.get(getAdapterPosition()));
                    }
                }
            });
        }
    }
}
