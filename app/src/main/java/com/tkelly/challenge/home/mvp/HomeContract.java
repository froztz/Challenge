package com.tkelly.challenge.home.mvp;

import android.net.Uri;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.StorageReference;

import com.tkelly.challenge.application.models.User;

import java.util.List;

/**
 * Created by Frostz on 8/19/2017.
 */

public interface HomeContract {

    interface View {
        void createAdapter();
        void update();
        void updateAdapter(List<User> userList);
        void sort(List<User> filteredList, String field, boolean isAscending);
    }

    interface Presenter {
        void onViewReady(DatabaseReference databaseReference);
        void goToProfile(User user);
        void onStart();
        void onPause();
        void onResume();
        void update();
        void addProfile(User user, Uri uri, DatabaseReference uRef, StorageReference sRef);
        void goToImageChooser();
        void sort(List<User> filteredList, String field, boolean isAscending);
        void undoSort(List<User> userList);
        void getData(DatabaseReference databaseReference);
        void filter(DatabaseReference databaseReference, Boolean isMale);
    }

    interface Navigator {
        void goToProfile(User user);
        void goToImageChooser();
    }

    interface Interactor{
        void getData(DatabaseReference databaseReference, OnDataRetrievedListener listener);
        void filter(DatabaseReference databaseReference, Boolean isMale, OnDataRetrievedListener listener);
        void addProfile(User user, Uri uri, DatabaseReference uRef, StorageReference sRef, OnDataRetrievedListener listener);
    }

    interface OnDataRetrievedListener{
        void onSuccess(List<User> userList);
        void onSuccess();
        void onError();
    }
}
