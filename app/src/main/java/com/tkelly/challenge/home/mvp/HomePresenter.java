package com.tkelly.challenge.home.mvp;

import android.content.Context;
import android.net.Uri;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.StorageReference;

import com.tkelly.challenge.application.models.Strings;
import com.tkelly.challenge.application.models.User;
import com.tkelly.challenge.home.HomeActivity;

import java.util.List;

/**
 * Created by Frostz on 8/18/2017.
 */

public class HomePresenter implements HomeContract.Presenter, HomeContract.OnDataRetrievedListener {

    private Context context;
    private HomeActivity activity;
    private final HomeContract.View view;
    private final HomeContract.Navigator navigator;
    private final HomeContract.Interactor interactor;

    public HomePresenter(Context context, HomeActivity activity, HomeContract.View view) {
        this.context = context;
        this.activity = activity;
        this.view = view;
        this.interactor = new HomeInteractor();
        this.navigator = new HomeNavigator(activity);
    }

    @Override
    public void onViewReady(DatabaseReference databaseReference) {
        activity.showDialog(Strings.LOADING);
        view.createAdapter();
        interactor.getData(databaseReference, this);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void getData(DatabaseReference databaseReference) {
        activity.showDialog(Strings.LOADING);
        interactor.getData(databaseReference, this);
    }

    @Override
    public void goToProfile(User user) {
        navigator.goToProfile(user);
    }

    @Override
    public void addProfile(User user, Uri uri, DatabaseReference uRef, StorageReference sRef) {
        activity.showDialog(Strings.LOADING);
        interactor.addProfile(user, uri, uRef, sRef, this);
    }

    @Override
    public void goToImageChooser() {
        navigator.goToImageChooser();
    }

    @Override
    public void sort(List<User> filteredList, String field, boolean isAscending) {
        view.sort(filteredList, field, isAscending);
    }

    @Override
    public void undoSort(List<User> userList) {
        view.updateAdapter(userList);
    }

    @Override
    public void filter(DatabaseReference databaseReference, Boolean isMale) {
        activity.showDialog(Strings.LOADING);
        interactor.filter(databaseReference, isMale, this);
    }

    @Override
    public void update() {
        view.update();
    }

    @Override
    public void onSuccess(List<User> userList) {
        activity.hideDialog();
        view.updateAdapter(userList);
    }

    @Override
    public void onSuccess() {
        activity.hideDialog();
    }

    @Override
    public void onError() {
        activity.hideDialog();
    }
}
