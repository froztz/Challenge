package com.tkelly.challenge.home.mvp;

import android.content.Intent;
import android.os.Bundle;

import com.tkelly.challenge.application.models.User;
import com.tkelly.challenge.home.HomeActivity;
import com.tkelly.challenge.profile.ProfileActivity;

import static com.tkelly.challenge.home.HomeActivity.REQUEST_CODE;

/**
 * Created by Frostz on 8/19/2017.
 */

public class HomeNavigator implements HomeContract.Navigator {

    private HomeActivity activity;

    public HomeNavigator(HomeActivity activity) {
        this.activity = activity;
    }

    @Override
    public void goToProfile(User user) {
        Intent intent = new Intent(activity, ProfileActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(ProfileActivity.USER, user);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    @Override
    public void goToImageChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        activity.startActivityForResult(Intent.createChooser(intent,
                "Select Picture"), REQUEST_CODE);
    }
}
