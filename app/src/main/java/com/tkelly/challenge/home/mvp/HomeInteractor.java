package com.tkelly.challenge.home.mvp;

import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import com.tkelly.challenge.application.models.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Frostz on 8/19/2017.
 */

public class HomeInteractor implements HomeContract.Interactor {

    @Override
    public void getData(DatabaseReference databaseReference, final HomeContract.OnDataRetrievedListener listener) {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                List<User> userList = new ArrayList<>();
                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    User user = noteDataSnapshot.getValue(User.class);
                    user.setKey(noteDataSnapshot.getKey());
                    userList.add(user);
                }
                listener.onSuccess(userList);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                listener.onError();
            }
        });


    }

    @Override
    public void filter(final DatabaseReference databaseReference, Boolean isMale, final HomeContract.OnDataRetrievedListener listener) {
        databaseReference.orderByChild("isMale").equalTo(isMale).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<User> userList = new ArrayList<>();
                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    User user = noteDataSnapshot.getValue(User.class);
                    userList.add(user);
                }
                listener.onSuccess(userList);
                databaseReference.removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                listener.onError();
            }
        });
    }

    @Override
    public void addProfile(final User user, Uri uri, final DatabaseReference uRef, final StorageReference sref, final HomeContract.OnDataRetrievedListener listener) {
        if (uri != null) {
            sref.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            user.setProfileImageUrl(taskSnapshot.getDownloadUrl().toString());
                            addProfileData(user, uRef, listener);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            listener.onError();
                        }
                    });
        } else {
            addProfileData(user,uRef,listener);
        }
    }

    private void addProfileData(final User user,DatabaseReference uRef, final HomeContract.OnDataRetrievedListener listener) {

        String id = uRef.push().getKey();
        DatabaseReference cRef = FirebaseDatabase.getInstance().getReference("uidGenerator");
        final DatabaseReference myRef2 = uRef.child(id);
        cRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                mutableData.setValue(mutableData.getValue() == null ? 1 : mutableData.getValue(Integer.class) + 1);
                user.setUid(mutableData.getValue(Long.class));
                myRef2.setValue(user);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                listener.onSuccess();
            }
        });
    }
}
