package com.tkelly.challenge.home;

import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.tkelly.challenge.ChallengeActivity;
import com.tkelly.challenge.ProfileItemClickListener;
import com.tkelly.challenge.ProfileRecyclerViewAdapter;
import com.tkelly.challenge.R;
import com.tkelly.challenge.application.models.Strings;
import com.tkelly.challenge.application.models.User;
import com.tkelly.challenge.home.mvp.HomeContract;
import com.tkelly.challenge.home.mvp.HomePresenter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends ChallengeActivity implements HomeContract.View, ProfileItemClickListener {

    @BindView(R.id.home_recycler_profiles)
    RecyclerView profileRecyclerView;
    @BindView(R.id.home_text_male)
    TextView mFilterTextView;
    @BindView(R.id.home_text_female)
    TextView fFilterTextView;
    @BindView(R.id.home_button_sort_by)
    ImageButton sortButton;
    @BindView(R.id.home_button_add)
    ImageButton addButton;


    public static final int REQUEST_CODE = 1001;
    public static final String FB_STORAGE_PATH = "image/";
    private HomePresenter presenter;
    private static final String TAG = "HomeActivity";
    private ProfileRecyclerViewAdapter adapter;
    private List<User> userList = new ArrayList<>();
    private TextView pSelectedTextView;
    private DatabaseReference uRef;
    private StorageReference sRef;
    private int sortby;
    private DialogViewHolder dialogViewHolder;
    private Uri selectedImage;

    @Override
    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
        super.onViewReady(savedInstanceState, intent);
        ButterKnife.bind(this);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        uRef = database.getReference("users");
        sRef = FirebaseStorage.getInstance().getReference();
        presenter.onViewReady(uRef);
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_home;
    }

    @Override
    protected void instantiatePresenter() {
        presenter = new HomePresenter(getApplicationContext(), this, this);
    }

    @Override
    public void createAdapter() {
        profileRecyclerView.setHasFixedSize(true);
        profileRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ProfileRecyclerViewAdapter(this, userList, this);
        profileRecyclerView.setAdapter(adapter);
    }

    @Override
    public void updateAdapter(List<User> userList) {
        this.userList = userList;
        if (sortby == 0 && pSelectedTextView == null) {
            adapter.refresh(userList);
        } else {
            presenter.update();
        }
    }

    @Override
    public void sort(List<User> filteredList, String field, final boolean isAscending) {
        List<User> temp = new ArrayList<>(filteredList != null ? filteredList : userList);
        if (field.equals(Strings.NAME)) {
            Collections.sort(temp, new Comparator<User>() {
                @Override
                public int compare(User o1, User o2) {
                    return isAscending ? o1.getName().compareTo(o2.getName()) : o2.getName().compareTo(o1.getName());
                }
            });
        } else {
            Collections.sort(temp, new Comparator<User>() {
                @Override
                public int compare(User o1, User o2) {
                    return isAscending ? o1.getAge().compareTo(o2.getAge()) : o2.getAge().compareTo(o1.getAge());
                }
            });
        }
        adapter.refresh(temp);
    }

    @Override
    public void update() {
        List<User> temp = null;
        if (pSelectedTextView != null) {
            temp = new ArrayList<>();
            boolean isMale = pSelectedTextView == mFilterTextView;
            for (User user : userList) {
                if (user.getIsMale() == isMale) {
                    temp.add(user);
                }
            }
        }
        if (sortby == 0){
            adapter.refresh(temp);
        } else {
            presenter.sort(temp, sortby < 3 ? Strings.AGE : Strings.NAME, sortby % 2 != 0);
        }
    }

    @Override
    public void onProfileItemClick(User user) {
        presenter.goToProfile(user);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE && data != null && data.getData() != null) {
            selectedImage = data.getData();
            dialogViewHolder.imageView.setImageURI(selectedImage);
        }
    }

    @OnClick(R.id.home_button_add)
    void addProfile() {
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_add_profile, null);
        dialogViewHolder = new DialogViewHolder(view);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.home_dialog_add_title)
                .setPositiveButton(R.string.home_dialog_add_add, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                })
                .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setView(view);
        final AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!dialogViewHolder.nameEditText.getText().toString().isEmpty() && !dialogViewHolder.ageEditText.getText().toString().isEmpty()) {
                    User user = new User();
                    user.setName(dialogViewHolder.nameEditText.getText().toString());
                    user.setAge(Double.valueOf(dialogViewHolder.ageEditText.getText().toString()));
                    user.setHobbies(!dialogViewHolder.hobbiesEditText.getText().toString().isEmpty() ? dialogViewHolder.hobbiesEditText.getText().toString() : "");
                    user.setIsMale(dialogViewHolder.genderRadioGroup.getCheckedRadioButtonId() == dialogViewHolder.maleRadioButton.getId());
                    StorageReference storageReferenceRef = selectedImage != null ? sRef.child(FB_STORAGE_PATH + System.currentTimeMillis() + "." + getImageExt(selectedImage)) : null;
                    presenter.addProfile(user, selectedImage, uRef, storageReferenceRef);
                    dialog.dismiss();
                } else {
                    dialog.setTitle(R.string.home_dialog_add_error);
                }
            }
        });
    }

    @OnClick(R.id.home_button_sort_by)
    void sortBy() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.home_sort_by)
                .setSingleChoiceItems(R.array.home_array_sortby, sortby, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (sortby == which) {
                            dialog.dismiss();
                        } else if (which == 0) {
                            sortby = which;
                            presenter.undoSort(userList);
                        } else {
                            sortby = which;
                            presenter.sort(null, which < 3 ? Strings.AGE : Strings.NAME, which % 2 != 0);
                        }
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

    @OnClick({R.id.home_text_male, R.id.home_text_female})
    void filter(TextView textView) {
        boolean isMale = textView.getId() == R.id.home_text_male;
        boolean isPrevious = pSelectedTextView == textView;
        if (pSelectedTextView != null && !isPrevious) {
            pSelectedTextView.setSelected(false);
        }
        textView.setSelected(!isPrevious);
        pSelectedTextView = isPrevious ? null : textView;

        if (!isPrevious) {
            presenter.filter(uRef, isMale);
        } else {
            presenter.getData(uRef);
        }
    }

    private String getImageExt(Uri uri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    public class DialogViewHolder {

        @BindView(R.id.dialog_add_edit_name)
        EditText nameEditText;
        @BindView(R.id.dialog_add_edit_age)
        EditText ageEditText;
        @BindView(R.id.dialog_add_edit_hobbies)
        EditText hobbiesEditText;
        @BindView(R.id.dialog_add_rg_gender)
        RadioGroup genderRadioGroup;
        @BindView(R.id.dialog_add_rb_male)
        RadioButton maleRadioButton;
        @BindView(R.id.dialog_add_rb_female)
        RadioButton femaleRadioButton;
        @BindView(R.id.dialog_add_image_profile)
        ImageView imageView;

        DialogViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        @OnClick(R.id.dialog_add_image_profile)
        void updateImage() {
            presenter.goToImageChooser();
        }
    }

}
