package com.tkelly.challenge.profile.mvp;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Frostz on 8/20/2017.
 */

public class ProfileInteractor implements ProfileContract.Interactor {

    @Override
    public void update(DatabaseReference uRef, String key, String bgColor, String hobbies, final ProfileContract.OnDataRetrievedListener listener) {
        if (bgColor == null && hobbies == null){
            listener.onSuccess();
        } else {
            Map updateMap = new HashMap();
            if (bgColor != null) {
                updateMap.put("backgroundColor", bgColor);
            }
            if (hobbies != null) {
                updateMap.put("hobbies", hobbies);
            }
            uRef.child(key).updateChildren(updateMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    listener.onSuccess();
                }
            });
        }
    }

    @Override
    public void remove(DatabaseReference uRef, String key, final ProfileContract.OnDataRetrievedListener listener) {
        uRef.child(key).removeValue(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                listener.onSuccess();
            }
        });
    }
}
