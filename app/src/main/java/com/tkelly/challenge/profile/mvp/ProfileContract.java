package com.tkelly.challenge.profile.mvp;

import com.google.firebase.database.DatabaseReference;

/**
 * Created by Frostz on 8/20/2017.
 */

public interface ProfileContract {
    interface View {
        void setup();
    }

    interface Presenter {
        void onViewReady();
        void onStart();
        void onPause();
        void onResume();
        void remove(DatabaseReference uRef, String key);
        void update(DatabaseReference uRef, String key, String bgColor, String hobbies);
    }

    interface Interactor{
        void update(DatabaseReference uRef, String key,String bgColor, String hobbies, OnDataRetrievedListener listener);
        void remove(DatabaseReference uRef, String key, OnDataRetrievedListener listener);
    }

    interface OnDataRetrievedListener{
        void onSuccess();
        void onError();
    }
}
