package com.tkelly.challenge.profile.mvp;


import android.content.Context;

import com.google.firebase.database.DatabaseReference;

import com.tkelly.challenge.application.models.Strings;
import com.tkelly.challenge.profile.ProfileActivity;

/**
 * Created by Frostz on 8/20/2017.
 */

public class ProfilePresenter implements ProfileContract.Presenter, ProfileContract.OnDataRetrievedListener {

    private Context context;
    private ProfileActivity activity;
    private final ProfileContract.View view;
    private final ProfileContract.Interactor interactor;

    public ProfilePresenter(Context context, ProfileActivity activity, ProfileContract.View view) {
        this.context = context;
        this.activity = activity;
        this.view = view;
        this.interactor = new ProfileInteractor();
    }

    @Override
    public void onViewReady() {
        view.setup();
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void remove(DatabaseReference uRef, String key) {
        activity.showDialog(Strings.LOADING);
        interactor.remove(uRef, key, this);
    }

    @Override
    public void update(DatabaseReference uRef, String key, String bgColor, String hobbies) {
        activity.showDialog(Strings.LOADING);
        interactor.update(uRef, key, bgColor, hobbies, this);
    }

    @Override
    public void onSuccess() {
        activity.hideDialog();
        activity.finish();
    }

    @Override
    public void onError() {
        activity.hideDialog();
    }
}
