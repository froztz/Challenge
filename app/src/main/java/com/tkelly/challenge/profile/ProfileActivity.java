package com.tkelly.challenge.profile;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.tkelly.challenge.ChallengeActivity;
import com.tkelly.challenge.R;
import com.tkelly.challenge.application.models.Strings;
import com.tkelly.challenge.application.models.User;
import com.tkelly.challenge.profile.mvp.ProfileContract;
import com.tkelly.challenge.profile.mvp.ProfilePresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Frostz on 8/20/2017.
 */

public class ProfileActivity extends ChallengeActivity implements ProfileContract.View, SeekBar.OnSeekBarChangeListener {

    @BindView(R.id.profile_layout_background)
    ScrollView bgScrollView;
    @BindView(R.id.profile_text_name)
    TextView nameTextView;
    @BindView(R.id.profile_text_age)
    TextView ageTextView;
    @BindView(R.id.profile_text_gender)
    TextView genderTextView;
    @BindView(R.id.profile_text_hobbies)
    TextView hobbyTextView;
    @BindView(R.id.profile_edit_hobbies)
    TextView hobbyEditText;
    @BindView(R.id.profile_image_profile)
    ImageView profileImageView;
    @BindView(R.id.profile_layout_edit)
    LinearLayout bgLinearLayout;
    @BindView(R.id.profile_layout_buttons)
    LinearLayout btLinearLayout;
    @BindView(R.id.profile_seek_red)
    SeekBar rSeekBar;
    @BindView(R.id.profile_seek_green)
    SeekBar gSeekBar;
    @BindView(R.id.profile_seek_blue)
    SeekBar bSeekBar;
    @BindView(R.id.profile_button_edit)
    Button editButton;
    @BindView(R.id.profile_button_remove)
    Button removeButton;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private ProfilePresenter presenter;
    private User user;
    private boolean isEdit;
    private DatabaseReference uRef;
    public static String USER = "user";


    @Override
    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
        super.onViewReady(savedInstanceState, intent);
        ButterKnife.bind(this);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            uRef = database.getReference("users");
            user = b.getParcelable(USER);
            presenter.onViewReady();
        } else {
            finish();
        }
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_profile;
    }

    @Override
    protected void instantiatePresenter() {
        presenter = new ProfilePresenter(getApplicationContext(), this, this);
    }

    @Override
    public void setup() {
        nameTextView.setText(user.getName());
        ageTextView.setText(String.valueOf(user.getAge().intValue()));
        genderTextView.setText(user.getIsMale() ? Strings.MALE : Strings.Female);
        hobbyTextView.setText(user.getHobbies() != null ? user.getHobbies() : "");
        if (user.getBackgroundColor() != null) {
            bgScrollView.setBackgroundColor(Color.parseColor(user.getBackgroundColor()));
            btLinearLayout.setBackgroundColor(Color.parseColor(user.getBackgroundColor()));
            ColorDrawable cd = (ColorDrawable) bgScrollView.getBackground();
            int color = cd.getColor();
            rSeekBar.setProgress(Color.red(color));
            gSeekBar.setProgress(Color.green(color));
            bSeekBar.setProgress(Color.blue(color));
        }
        if (user.getProfileImageUrl() != null) {
            Glide.with(this)
                    .load(user.getProfileImageUrl())
                    .into(profileImageView);
        }
        rSeekBar.setOnSeekBarChangeListener(this);
        gSeekBar.setOnSeekBarChangeListener(this);
        bSeekBar.setOnSeekBarChangeListener(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        bgScrollView.setBackgroundColor(Color.rgb(rSeekBar.getProgress(), gSeekBar.getProgress(), bSeekBar.getProgress()));
        btLinearLayout.setBackgroundColor(Color.rgb(rSeekBar.getProgress(), gSeekBar.getProgress(), bSeekBar.getProgress()));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    @OnClick({R.id.profile_button_edit, R.id.profile_button_remove})
    void onClick(Button button) {
        if (button.getId() == R.id.profile_button_edit) {
            isEdit = !isEdit;
            bgLinearLayout.setVisibility(isEdit ? View.VISIBLE : View.GONE);
            hobbyEditText.setText(isEdit ? hobbyTextView.getText().toString() : "");
            hobbyTextView.setVisibility(isEdit ? View.GONE : View.VISIBLE);
            editButton.setText(isEdit ? R.string.profile_button_cancel : R.string.profile_button_edit);
            removeButton.setText(isEdit ? R.string.profile_button_done : R.string.profile_button_remove);
            bgScrollView.setBackgroundColor(user.getBackgroundColor() != null ? Color.parseColor(user.getBackgroundColor()) :  Color.WHITE);
            btLinearLayout.setBackgroundColor(user.getBackgroundColor() != null ? Color.parseColor(user.getBackgroundColor()) :  Color.WHITE);
            ColorDrawable cd = (ColorDrawable) bgScrollView.getBackground();
            int color = cd.getColor();
            rSeekBar.setProgress(user.getBackgroundColor() != null ? Color.red(color) :  255);
            gSeekBar.setProgress(user.getBackgroundColor() != null ? Color.green(color) :  255);
            bSeekBar.setProgress(user.getBackgroundColor() != null ? Color.blue(color) :  255);
        }
        else {
            if (isEdit){
                String old = hobbyTextView.getText().toString();
                String nHobby = hobbyEditText.getText().toString();
                int color = ((ColorDrawable) bgScrollView.getBackground()).getColor();
                String hexColor = String.format("#%06X", (0xFFFFFF & color));
                presenter.update(uRef, user.getKey(), !hexColor.equals(user.getBackgroundColor()) ? hexColor : null , !old.equals(nHobby) && !nHobby.isEmpty() ? nHobby : null);
            } else {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.profile_button_remove)
                        .setMessage(R.string.profile_dialog_message)
                        .setPositiveButton(R.string.profile_button_remove, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                presenter.remove(uRef, user.getKey());
                            }
                        })
                        .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                .create();
                builder.show();
            }
        }
    }
}
